package com.example.shahboz.odc2018.init;

import android.app.Application;

import com.example.shahboz.odc2018.cache.PrefManeger;


public class app extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        PrefManeger.init(this);
    }
}
