package com.example.shahboz.odc2018.cache;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefManeger {
    private SharedPreferences pref;
    private static PrefManeger maneger;

    public static PrefManeger getManeger() {
        return maneger;
    }

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "fozilbek_welcome";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    private PrefManeger(Context context) {
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
    }

    public static void init(Context context) {
        if (maneger == null) {
            maneger = new PrefManeger(context);
        }
    }


    public void setFirstTimeLaunch(boolean isFirstTime) {
        pref.edit().putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime).apply();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }
}
