package com.example.shahboz.odc2018.karti;

import android.annotation.SuppressLint;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.shahboz.odc2018.R;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity implements
        OnMapReadyCallback, PermissionsListener,
        MapboxMap.OnMapClickListener, MapboxMap.OnMarkerClickListener {


    private MapView mapView;
    private MapboxMap mapboxMap;
    private PermissionsManager permissionsManager;
    private Location originLocation;
    private Marker destinationMarker;
    private Marker bir;
    private Marker ikkk_mar;
    private Marker uch_mar;
    private Marker turt_mar;
    private Marker besh_mar;
    private LatLng originCoord;
    private LatLng destinationCoord;
    private LatLng endLatLng;

    private Point originPosition;
    private Point destinationPosition;
    private DirectionsRoute currentRoute;
    private static final String TAG = "DirectionsActivity";
    private NavigationMapRoute navigationMapRoute;


    private Button button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, getString(R.string.access_token));
        setContentView(R.layout.activity_main);
        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);


    }

    private void loadMarkers() {

        mapView.getMapAsync(mapboxMap -> {
            mapboxMap.addMarker(new MarkerOptions()
                    .position(new LatLng(41.341808, 69.333906))
                    .title("Tuit")
                    .snippet("Inha")
            );
        });
    }

    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent() {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            // Activate the MapboxMap LocationComponent to show user location
            // Adding in LocationComponentOptions is also an optional parameter
            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            locationComponent.activateLocationComponent(this);
            locationComponent.setLocationComponentEnabled(true);
            // Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);
            originLocation = locationComponent.getLastKnownLocation();

        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocationComponent();
        } else {
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onMapClick(@NonNull LatLng point) {
//        if (destinationMarker != null) {
//            mapboxMap.removeMarker(destinationMarker);
//        }
//        destinationCoord = point;
//        destinationMarker = mapboxMap.addMarker(new MarkerOptions()
//                .position(destinationCoord)
//        );
//        destinationPosition = Point.fromLngLat(destinationCoord.getLongitude(),
//                destinationCoord.getLatitude());
//        originPosition = Point.fromLngLat(originCoord.getLongitude(), originCoord.getLatitude());
//        getRoute(originPosition, destinationPosition);

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        enableLocationComponent();


        this.mapboxMap.setOnMarkerClickListener(new MapboxMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(@NonNull Marker marker) {
                if (marker.getTitle().equals("Restoran Sayyor")) {
                    final Point last = Point.fromLngLat(69.317750, 41.318570);
                    originPosition = Point.fromLngLat(originCoord.getLongitude(), originCoord.getLatitude());
                    getRoute(originPosition, last);
                    return false;
                }
                if (marker.getTitle().equals("Cafe vachach")) {
                    final Point last = Point.fromLngLat(69.314408, 41.318070);
                    originPosition = Point.fromLngLat(originCoord.getLongitude(), originCoord.getLatitude());
                    getRoute(originPosition, last);
                    return false;
                }
                if (marker.getTitle().equals("April")) {
                    final Point last = Point.fromLngLat(69.304944, 41.355744);
                    originPosition = Point.fromLngLat(originCoord.getLongitude(), originCoord.getLatitude());
                    getRoute(originPosition, last);
                    return false;
                }
                if (marker.getTitle().equals("Marrakesh")) {
                    final Point last = Point.fromLngLat(69.284651, 41.331518);
                    originPosition = Point.fromLngLat(originCoord.getLongitude(), originCoord.getLatitude());
                    getRoute(originPosition, last);
                    return false;
                }
                if (marker.getTitle().equals("Na Nebe")) {
                    final Point last = Point.fromLngLat(69.278105, 41.328120);
                    originPosition = Point.fromLngLat(originCoord.getLongitude(), originCoord.getLatitude());
                    getRoute(originPosition, last);
                    return false;
                }
                return false;
            }

        });

        originCoord = new LatLng(originLocation.getLatitude(),
                originLocation.getLongitude());
        mapboxMap.addOnMapClickListener(this);

        //  Icon icon = IconFactory.getInstance(MainActivity.this).fromResource(R.drawable.ic_location_on_black_24dp);


        ikkk_mar = mapboxMap.addMarker(new MarkerOptions()
                .position(new LatLng(41.318570, 69.317750))
                .setTitle("Restoran Sayyor")
                .setSnippet("Ресторан Sayyor, 11 Mirzo Ulugbek Avenue, Tashkent, O`zbekiston"));


        uch_mar = mapboxMap.addMarker(new MarkerOptions()
                .position(new LatLng(41.318070, 69.314408))
                .setTitle("Cafe vachach")

                .setSnippet("Cafe \"vachach\", 51 Parkent Street, Tashkent 700007, O`zbekiston"));

        turt_mar = mapboxMap.addMarker(new MarkerOptions()
                .position(new LatLng(41.355744, 69.304944))
                .setTitle("April")

                .setSnippet("April Verdant Restaurant (Building 2), Chinobod Street, Tashkent, O`zbekiston"));

        besh_mar = mapboxMap.addMarker(new MarkerOptions()
                .position(new LatLng(41.331518, 69.284651))
                .setTitle("Marrakesh")

                .setSnippet("Тошкент, O`zbekiston"));

        besh_mar = mapboxMap.addMarker(new MarkerOptions()
                .position(new LatLng(41.328120, 69.278105))
                .setTitle("Na Nebe")

                .setSnippet("57 Xurshid ko'chasi, Тошкент, O`zbekiston"));


    }


    private void getRoute(Point origin, Point destination) {
        NavigationRoute.builder(this)
                .accessToken(Mapbox.getAccessToken())
                .origin(origin)
                .destination(destination)
                .build()
                .getRoute(new Callback<DirectionsResponse>() {
                    @Override
                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                        // You can get the generic HTTP info about the response
                        Log.d(TAG, "Response code: " + response.code());
                        if (response.body() == null) {
                            Log.e(TAG, "No routes found, make sure you set the right user and access token.");
                            return;
                        } else if (response.body().routes().size() < 1) {
                            Log.e(TAG, "No routes found");
                            return;
                        }

                        currentRoute = response.body().routes().get(0);


                        // Draw the route on the map
                        if (navigationMapRoute != null) {
                            navigationMapRoute.removeRoute();
                        } else {

                            navigationMapRoute = new NavigationMapRoute(null, mapView, mapboxMap, R.style.NavigationMapRoute);

                        }

                        navigationMapRoute.addRoute(currentRoute);

                    }

                    @Override
                    public void onFailure(Call<DirectionsResponse> call, Throwable throwable) {
                        Log.e(TAG, "Error: " + throwable.getMessage());
                    }
                });
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {

        if (marker.equals(ikkk_mar)) {
            final Point last = Point.fromLngLat(41.357071, 69.304995);
            originPosition = Point.fromLngLat(originCoord.getLongitude(), originCoord.getLatitude());
            getRoute(originPosition, last);
            return false;

        }
        return false;
    }


    @SuppressLint("MissingPermission")
    public void FOCUS(View view) {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            locationComponent.activateLocationComponent(this);
            locationComponent.setLocationComponentEnabled(true);
            // Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);
            originLocation = locationComponent.getLastKnownLocation();

        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }
}